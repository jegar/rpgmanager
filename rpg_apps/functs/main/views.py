from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.db import IntegrityError
from django.contrib.auth.decorators import login_required
from rpg_apps.functs.main.models import RPG, Game, UserGame
from rpg_apps.functs.main.forms import UserForm, UserProfileForm, NewGameForm


def index(request):
    rpgs = RPG.objects.all()
    return render(request, 'functs/main/index.html', {'rpgs': rpgs})


def register(request):
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        user_profile_form = UserProfileForm(data=request.POST)
        if user_form.is_valid() and user_profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            user_profile = user_profile_form.save(commit=False)
            user_profile.user = user
            user_profile.save()
            return HttpResponseRedirect('/main/login')
        else:
            if not user_form.is_valid():
                print user_form.errors
            if not user_profile_form.is_valid():
                print user_profile_form.errors
    else:
        user_form = UserForm()
        user_profile_form = UserProfileForm()
    context_dict = {'user_form': user_form,
                    'user_profile_form': user_profile_form}
    return render(request, 'functs/main/register.html', context_dict)


def games(request):
    user = request.user
    context_dict = {}
    games_all = Game.objects.all().values(
        'rpg__name', 'rpg__simg', 'rpg__name', 'user__username',
        'name', 'desc', 'next_date', 'pk')
    context_dict['games'] = games_all
    if user.is_authenticated():
        pass
    else:
        pass
    return render (request, 'functs/main/games.html', context_dict)


def game_properties(request, id):
    try:
        game = Game.objects.get(pk=id)
        usergame = UserGame.objects.get(game=game, user=request.user).values()
        rpg = game.select_related('rpg')
        owner = game.select_related('user')
        if request.POST:
            pass
        else:
            pass
        players = UserGame.objects.filter(game=game)
        context_dict = {'game' : game, 'players' : players,
                        'usergame' : usergame, 'rpg' : rpg, 'owner': owner}
        return render(request, 'functs/main/games_properties.html', context_dict)
    except Exception as e:
        print e
        return HttpResponseRedirect('/main/games')


@login_required
def new_game(request):
    context_dict = {}
    if request.method == 'POST':
        newgame_form = NewGameForm(data=request.POST)
        if newgame_form.is_valid():
            try:
                newg = newgame_form.save(commit=False)
                newg.user = request.user
                newg.save()
                user_game = UserGame()
                user_game.game = newg
                user_game.user = request.user
                user_game.permissions = UserGame.CREATOR
                user_game.save()
            except IntegrityError:
                context_dict['integrity_error'] = 'That game name already exists.'
            else:
                HttpResponseRedirect('/games/')
        else:
            print newgame_form.errors
    else:
        newgame_form = NewGameForm()
    context_dict['newgame_form'] = newgame_form
    return render(request, 'functs/main/newgame.html', context_dict)


@login_required
def send(request):
    if request.method == 'POST':
        send_form = SendMessageForm(data=request.POST)
        if send_form.is_valid():
            send_form.save()
            send_form = SendMessageForm()
        else:
            print send_form.errors
    else:
        send_form = SendMessageForm()
    context_dict = {'send_form': send_form }
    return render(request, 'functs/mail/send.html', context_dict)


@login_required
def invites(request):
    if request.method == 'POST':
        inv_pk = request.POST['game']
        try:
            invite = Invite.objects.get(game=int(inv_pk), user_to=request.user)
            game = UserGame.objects.get(pk=invite.game)
            if 'invite_accept' in request.POST:
                text = "The user " + request.user + " has accepted" + \
                       " your invitation for the game " + inv_pk
                game.accepted = True
                game.save()
            else:
                text = "The user " + request.user + " has declined" + \
                        " your invitation for the game " + inv_pk
                game.delete()
            invite.delete()
        except Invite.DoesNotExist:
            pass
    invites = Invite.objects.filter(user_to=request.user)\
        .values('pk','user_from__username','game__game__name', 'date')
    context_dict = {'invites': invites}
    return render(request, 'functs/mail/invites.html', context_dict)


@login_required
def invite_game(request):
    context_dict = {}
    if request.method == 'POST':
        print request.POST
        invite_form = InviteGameForm(current_user=request.user, data=request.POST)
        if invite_form.is_valid():
            user_game = invite_form.save()
            message = Invite(user_from=request.user,
                        user_to=user_game.user, game=user_game)
            message.save()
            context_dict['msg_success'] = "User invite sent."
            invite_form = InviteGameForm(current_user=request.user)
        else:
            print invite_form.errors
    else:
        invite_form = InviteGameForm(current_user=request.user)
    context_dict['invite_form'] = invite_form
    return render(request, 'functs/mail/invitegame.html', context_dict)