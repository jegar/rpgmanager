"""RPGManager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from rpg_apps.functs.main import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^index$', views.index, name='index'),
    url(r'^login$', auth_views.LoginView.as_view(template_name='functs/main/login.html',
                                                redirect_field_name='/main/games'),
        name='login'),
    url(r'logout', auth_views.LogoutView.as_view(next_page='/main/index'),
        name='logout'),
    url(r'^register$', views.register, name='register'),
    url(r'^games$', views.games, name='games'),
    url(r'^games/([0-9]+)$', views.game_properties, name='games'),
    url(r'^new_game$', views.new_game, name='new_game'),
    url(r'^send$', views.send, name='send'),
    url(r'^invite$', views.invites, name='invite'),
    url(r'^invite_game$', views.invite_game, name='invite_game'),
]
# + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


