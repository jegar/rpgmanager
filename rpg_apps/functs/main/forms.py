from django import forms
from django.forms import widgets
from django.contrib.auth.models import User
from rpg_apps.functs.main.models import UserProfile, Game, Message, UserGame


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ('username', 'password', 'email')


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        exclude = ('user',)


class NewGameForm(forms.ModelForm):
    class Meta:
        model = Game
        fields = ('name', 'rpg', 'desc')
    date = forms.DateField(
        widget=widgets.SelectDateWidget(empty_label=('Year', 'Month', 'Day'))
    )
    time = forms.TimeField(widget=widgets.TimeInput())

    def save(self, commit=True):
        self.cleaned_data['next_data'] = self.cleaned_data['date'] + ' ' + self.cleaned_data['time']
        super(NewGameForm, self).save(commit=commit)


class SendMessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('user_to', 'text')


class InviteGameForm(forms.ModelForm):

    def __init__(self, current_user, *args, **kwargs):
        super(InviteGameForm, self).__init__(*args, **kwargs)
        self.fields['user'].queryset = self.fields['user'].queryset.exclude(id=current_user.id)
        self.fields['game'].queryset = self.fields['game'].queryset.filter(user=current_user.id)

    class Meta:
        model = UserGame
        fields = ('game', 'user')
    permissions = forms.ChoiceField(choices=((UserGame.OBSERVER, 'Observer'),
                    (UserGame.PLAYER, 'Player'),(UserGame.CONTRIB, 'Contrib')))

