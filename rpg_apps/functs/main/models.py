from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    """This model is an extension of the default User model,
        its purpose is to hide the username of the user in order
        to improve security a little."""
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nickname = models.CharField(max_length=51, primary_key=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)


class RPG(models.Model):
    """This models contains all the games that are usable in the app."""
    slug = models.SlugField(max_length=10, blank=False, unique=True)
    name = models.CharField(max_length=100, blank=False)
    img = models.ImageField(upload_to='main', blank=False)
    simg = models.ImageField(upload_to='main', blank=False)
    desc = models.TextField(max_length=255)

    def __str__(self):
        return self.name

class UserGame(models.Model):
    OBSERVER = 0
    PLAYER = 1
    CONTRIB = 2
    OWNER = 3
    game = models.ForeignKey("Game", blank=False)
    user = models.ForeignKey(User, blank=False, on_delete=models.CASCADE)
    permissions = models.IntegerField(default=0, blank=False, choices=(
                        (OBSERVER, 'Observer'), (PLAYER, 'Player'), (CONTRIB, 'Contrib'), (OWNER, 'Owner')))
    accepted = models.BooleanField(default=False)

    class Meta:
        unique_together = ('game', 'user')


class Game(models.Model):
    owner = models.ForeignKey(UserGame, blank=False, on_delete=models.CASCADE, related_name="game_owner")
    name = models.CharField(max_length=100, blank=False)
    rpg = models.ForeignKey(RPG, blank=False)
    desc = models.TextField(max_length=255, default=None)
    next_date = models.DateTimeField(blank=True, null=True, default=None)

    def __str__(self):
        return self.name + " --- " + self.rpg.__str__()

    class Meta:
        unique_together = ('owner', 'name', 'rpg')


class Message(models.Model):
    user_from = models.ForeignKey(User, blank=False, related_name="msg_orig")
    user_to = models.ForeignKey(User, blank=False, related_name="msg_dest")
    date = models.DateTimeField(auto_now_add=True, blank=False)
    text = models.CharField(max_length=255, blank=False)
    seen = models.BooleanField(default=False)


class Invite(models.Model):
    user_from = models.ForeignKey(User, blank=False, related_name="inv_orig")
    user_to = models.ForeignKey(User, blank=False, related_name="inv_dest")
    game = models.ForeignKey(UserGame, blank=False)
    date = models.DateTimeField(auto_now_add=True, blank=False)
